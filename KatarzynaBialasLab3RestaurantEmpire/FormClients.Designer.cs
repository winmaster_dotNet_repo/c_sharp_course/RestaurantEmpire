﻿namespace KatarzynaBialasLab3RestaurantEmpire
{
    partial class FormClients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewClients = new System.Windows.Forms.DataGridView();
            this.labelClientList = new System.Windows.Forms.Label();
            this.buttonShowClients = new System.Windows.Forms.Button();
            this.labelClientOrdersList = new System.Windows.Forms.Label();
            this.dataGridViewClientsOrdersList = new System.Windows.Forms.DataGridView();
            this.buttonShowClientsOrders = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClientsOrdersList)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewClients
            // 
            this.dataGridViewClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClients.Location = new System.Drawing.Point(41, 67);
            this.dataGridViewClients.Name = "dataGridViewClients";
            this.dataGridViewClients.Size = new System.Drawing.Size(353, 166);
            this.dataGridViewClients.TabIndex = 0;
            // 
            // labelClientList
            // 
            this.labelClientList.AutoSize = true;
            this.labelClientList.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.labelClientList.Location = new System.Drawing.Point(36, 39);
            this.labelClientList.Name = "labelClientList";
            this.labelClientList.Size = new System.Drawing.Size(132, 25);
            this.labelClientList.TabIndex = 1;
            this.labelClientList.Text = "Lista Klientow";
            // 
            // buttonShowClients
            // 
            this.buttonShowClients.Location = new System.Drawing.Point(41, 239);
            this.buttonShowClients.Name = "buttonShowClients";
            this.buttonShowClients.Size = new System.Drawing.Size(353, 23);
            this.buttonShowClients.TabIndex = 2;
            this.buttonShowClients.Text = "Pokaż Klientów:";
            this.buttonShowClients.UseVisualStyleBackColor = true;
            this.buttonShowClients.Click += new System.EventHandler(this.buttonShowClients_Click);
            // 
            // labelClientOrdersList
            // 
            this.labelClientOrdersList.AutoSize = true;
            this.labelClientOrdersList.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.labelClientOrdersList.Location = new System.Drawing.Point(36, 305);
            this.labelClientOrdersList.Name = "labelClientOrdersList";
            this.labelClientOrdersList.Size = new System.Drawing.Size(148, 25);
            this.labelClientOrdersList.TabIndex = 3;
            this.labelClientOrdersList.Text = "Lista Zamówień";
            // 
            // dataGridViewClientsOrdersList
            // 
            this.dataGridViewClientsOrdersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClientsOrdersList.Location = new System.Drawing.Point(41, 333);
            this.dataGridViewClientsOrdersList.Name = "dataGridViewClientsOrdersList";
            this.dataGridViewClientsOrdersList.Size = new System.Drawing.Size(353, 166);
            this.dataGridViewClientsOrdersList.TabIndex = 4;
            // 
            // buttonShowClientsOrders
            // 
            this.buttonShowClientsOrders.Location = new System.Drawing.Point(41, 505);
            this.buttonShowClientsOrders.Name = "buttonShowClientsOrders";
            this.buttonShowClientsOrders.Size = new System.Drawing.Size(353, 23);
            this.buttonShowClientsOrders.TabIndex = 5;
            this.buttonShowClientsOrders.Text = "Pokaż Liste zamówień:";
            this.buttonShowClientsOrders.UseVisualStyleBackColor = true;
            this.buttonShowClientsOrders.Click += new System.EventHandler(this.buttonShowClientsOrders_Click);
            // 
            // FormClients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 572);
            this.Controls.Add(this.buttonShowClientsOrders);
            this.Controls.Add(this.dataGridViewClientsOrdersList);
            this.Controls.Add(this.labelClientOrdersList);
            this.Controls.Add(this.buttonShowClients);
            this.Controls.Add(this.labelClientList);
            this.Controls.Add(this.dataGridViewClients);
            this.Name = "FormClients";
            this.Text = "FormClients";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClientsOrdersList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewClients;
        private System.Windows.Forms.Label labelClientList;
        private System.Windows.Forms.Button buttonShowClients;
        private System.Windows.Forms.Label labelClientOrdersList;
        private System.Windows.Forms.DataGridView dataGridViewClientsOrdersList;
        private System.Windows.Forms.Button buttonShowClientsOrders;
    }
}