﻿namespace KatarzynaBialasLab3RestaurantEmpire
{
    partial class FormRestaurantEmpire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelName = new System.Windows.Forms.Label();
            this.dataGridViewMenu = new System.Windows.Forms.DataGridView();
            this.buttonShowMenu = new System.Windows.Forms.Button();
            this.labelMenu = new System.Windows.Forms.Label();
            this.dataGridViewMenuCategory = new System.Windows.Forms.DataGridView();
            this.labelCategory = new System.Windows.Forms.Label();
            this.buttonGetCategory = new System.Windows.Forms.Button();
            this.labelManagement = new System.Windows.Forms.Label();
            this.buttonWorkers = new System.Windows.Forms.Button();
            this.buttonClients = new System.Windows.Forms.Button();
            this.pictureBoxDishImage = new System.Windows.Forms.PictureBox();
            this.buttonShowPicture = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMenuCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDishImage)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Monotype Corsiva", 30F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.ForeColor = System.Drawing.Color.IndianRed;
            this.labelName.Location = new System.Drawing.Point(104, 9);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(341, 49);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Restauracja \"Empire\"";
            // 
            // dataGridViewMenu
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Monotype Corsiva", 11F, System.Drawing.FontStyle.Italic);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewMenu.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewMenu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewMenu.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewMenu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Monotype Corsiva", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewMenu.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewMenu.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewMenu.Location = new System.Drawing.Point(12, 123);
            this.dataGridViewMenu.Name = "dataGridViewMenu";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewMenu.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewMenu.Size = new System.Drawing.Size(240, 293);
            this.dataGridViewMenu.TabIndex = 1;
            // 
            // buttonShowMenu
            // 
            this.buttonShowMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonShowMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonShowMenu.ForeColor = System.Drawing.Color.DarkBlue;
            this.buttonShowMenu.Location = new System.Drawing.Point(12, 446);
            this.buttonShowMenu.Name = "buttonShowMenu";
            this.buttonShowMenu.Size = new System.Drawing.Size(240, 31);
            this.buttonShowMenu.TabIndex = 2;
            this.buttonShowMenu.Text = "Wyświetl Menu";
            this.buttonShowMenu.UseVisualStyleBackColor = false;
            this.buttonShowMenu.Click += new System.EventHandler(this.buttonShowMenu_Click);
            // 
            // labelMenu
            // 
            this.labelMenu.AutoSize = true;
            this.labelMenu.Font = new System.Drawing.Font("Mistral", 25F);
            this.labelMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelMenu.Location = new System.Drawing.Point(64, 79);
            this.labelMenu.Name = "labelMenu";
            this.labelMenu.Size = new System.Drawing.Size(84, 41);
            this.labelMenu.TabIndex = 3;
            this.labelMenu.Text = "Menu:";
            // 
            // dataGridViewMenuCategory
            // 
            this.dataGridViewMenuCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMenuCategory.Location = new System.Drawing.Point(284, 123);
            this.dataGridViewMenuCategory.Name = "dataGridViewMenuCategory";
            this.dataGridViewMenuCategory.Size = new System.Drawing.Size(285, 150);
            this.dataGridViewMenuCategory.TabIndex = 4;
            // 
            // labelCategory
            // 
            this.labelCategory.AutoSize = true;
            this.labelCategory.Font = new System.Drawing.Font("Mistral", 20F);
            this.labelCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelCategory.Location = new System.Drawing.Point(344, 87);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(101, 33);
            this.labelCategory.TabIndex = 5;
            this.labelCategory.Text = "Kategoria:";
            // 
            // buttonGetCategory
            // 
            this.buttonGetCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonGetCategory.Enabled = false;
            this.buttonGetCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonGetCategory.ForeColor = System.Drawing.Color.DarkBlue;
            this.buttonGetCategory.Location = new System.Drawing.Point(12, 483);
            this.buttonGetCategory.Name = "buttonGetCategory";
            this.buttonGetCategory.Size = new System.Drawing.Size(240, 31);
            this.buttonGetCategory.TabIndex = 6;
            this.buttonGetCategory.Text = "Wyświetl kategorię";
            this.buttonGetCategory.UseVisualStyleBackColor = false;
            this.buttonGetCategory.Click += new System.EventHandler(this.buttonGetCategory_Click);
            // 
            // labelManagement
            // 
            this.labelManagement.AutoSize = true;
            this.labelManagement.Font = new System.Drawing.Font("Modern No. 20", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelManagement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelManagement.Location = new System.Drawing.Point(403, 471);
            this.labelManagement.Name = "labelManagement";
            this.labelManagement.Size = new System.Drawing.Size(166, 29);
            this.labelManagement.TabIndex = 9;
            this.labelManagement.Text = "Zarządzanie:";
            // 
            // buttonWorkers
            // 
            this.buttonWorkers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonWorkers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonWorkers.ForeColor = System.Drawing.Color.DarkBlue;
            this.buttonWorkers.Location = new System.Drawing.Point(419, 511);
            this.buttonWorkers.Name = "buttonWorkers";
            this.buttonWorkers.Size = new System.Drawing.Size(150, 31);
            this.buttonWorkers.TabIndex = 10;
            this.buttonWorkers.Text = "Pracownicy";
            this.buttonWorkers.UseVisualStyleBackColor = false;
            this.buttonWorkers.Click += new System.EventHandler(this.buttonWorkers_Click);
            // 
            // buttonClients
            // 
            this.buttonClients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonClients.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonClients.ForeColor = System.Drawing.Color.DarkBlue;
            this.buttonClients.Location = new System.Drawing.Point(419, 546);
            this.buttonClients.Name = "buttonClients";
            this.buttonClients.Size = new System.Drawing.Size(150, 31);
            this.buttonClients.TabIndex = 11;
            this.buttonClients.Text = "Klienci";
            this.buttonClients.UseVisualStyleBackColor = false;
            this.buttonClients.Click += new System.EventHandler(this.buttonClients_Click);
            // 
            // pictureBoxDishImage
            // 
            this.pictureBoxDishImage.Location = new System.Drawing.Point(284, 301);
            this.pictureBoxDishImage.Name = "pictureBoxDishImage";
            this.pictureBoxDishImage.Size = new System.Drawing.Size(200, 146);
            this.pictureBoxDishImage.TabIndex = 12;
            this.pictureBoxDishImage.TabStop = false;
            // 
            // buttonShowPicture
            // 
            this.buttonShowPicture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonShowPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonShowPicture.ForeColor = System.Drawing.Color.DarkBlue;
            this.buttonShowPicture.Location = new System.Drawing.Point(499, 322);
            this.buttonShowPicture.Name = "buttonShowPicture";
            this.buttonShowPicture.Size = new System.Drawing.Size(70, 62);
            this.buttonShowPicture.TabIndex = 13;
            this.buttonShowPicture.Text = "Pokaż obraz";
            this.buttonShowPicture.UseVisualStyleBackColor = false;
            this.buttonShowPicture.Click += new System.EventHandler(this.buttonShowPicture_Click);
            // 
            // FormRestaurantEmpire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(581, 589);
            this.Controls.Add(this.buttonShowPicture);
            this.Controls.Add(this.pictureBoxDishImage);
            this.Controls.Add(this.buttonClients);
            this.Controls.Add(this.buttonWorkers);
            this.Controls.Add(this.labelManagement);
            this.Controls.Add(this.buttonGetCategory);
            this.Controls.Add(this.labelCategory);
            this.Controls.Add(this.dataGridViewMenuCategory);
            this.Controls.Add(this.labelMenu);
            this.Controls.Add(this.buttonShowMenu);
            this.Controls.Add(this.dataGridViewMenu);
            this.Controls.Add(this.labelName);
            this.Name = "FormRestaurantEmpire";
            this.Text = "Restauracja";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMenuCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDishImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridViewMenu;
        private System.Windows.Forms.Button buttonShowMenu;
        private System.Windows.Forms.Label labelMenu;
        private System.Windows.Forms.DataGridView dataGridViewMenuCategory;
        private System.Windows.Forms.Label labelCategory;
        private System.Windows.Forms.Button buttonGetCategory;
        private System.Windows.Forms.Label labelManagement;
        private System.Windows.Forms.Button buttonWorkers;
        private System.Windows.Forms.Button buttonClients;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.PictureBox pictureBoxDishImage;
        private System.Windows.Forms.Button buttonShowPicture;
    }
}

